import Footer from './components/footer/Footer';
import Head from './components/head/Head';
import Main from './components/main/Main';

function App() {
  return (
    <div className="App">
      <Head />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
