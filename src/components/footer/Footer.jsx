import React from 'react';

const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <div className="footer-row">
          <div className="footer-copyright">
            <div className="footer-copyright-name">© Зураб Тхакохов</div>
            <p>
              HTML верстка и разработка сайтов с использованием React,
              TypeScript
            </p>
            <p>
              Github:{' '}
              <a
                rel="noreferrer"
                target="_blank"
                href="https://github.com/Zurik09"
              >
                https://github.com/Zurik09
              </a>{' '}
            </p>
            <p>
              GitLab:{' '}
              <a
                rel="noreferrer"
                target="_blank"
                href="https://gitlab.com/zurik"
              >
                https://gitlab.com/zurik
              </a>{' '}
            </p>
          </div>
          <div className="footer-contacts">
            {/* <a href="/" className="footer-button">
              Связаться Вконтакте
            </a> */}
            <p>Tel: +7-928-394-3304</p>
            <p>Email: zurik09@mail.ru</p>
            <p>
              Напишите мне, чтобы заказать или узнать стоимость верстки вашего
              проекта
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
