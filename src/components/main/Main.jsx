import React from 'react';

import romaPizza from './../../assets/img/portfolio/romapizza.jpg';
import githubSearch from './../../assets/img/portfolio/githubSearch.jpg';
import slider from './../../assets/img/portfolio/slider.jpg';
import cards from './../../assets/img/portfolio/cards.jpg';

const Main = () => {
  return (
    <div>
      <section id="portfolio" className="portfolio">
        <div className="container">
          <h2 className="portfolio-header">Портфолио</h2>

          <div className="portfolio-card-wrapper">
            <div className="card">
              <a
                rel="noreferrer"
                href="https://zurik09.github.io/roma-pizza/"
                className="card-link"
                target="_blank"
              >
                <img className="card-img" src={romaPizza} alt="roma-pizza" />
                <h3 className="card-title">
                  SPA - приложение для заказа Пиццы
                </h3>
                <p>Стек: React, TypeScript, Redux Toolkit, SCSS, адаптив</p>
              </a>
            </div>

            <div className="card">
              <a
                rel="noreferrer"
                href="https://github-search-puce-ten.vercel.app/"
                className="card-link"
                target="_blank"
              >
                <img
                  className="card-img"
                  src={githubSearch}
                  alt="githubSearch"
                />
                <h3 className="card-title">SPA - приложение GithubSearch</h3>
                <p>Стек: React, Redux Toolkit, SCSS</p>
              </a>
            </div>

            <div className="card">
              <a
                rel="noreferrer"
                href="https://slider-drab.vercel.app/"
                className="card-link"
                target="_blank"
              >
                <img className="card-img" src={slider} alt="slider" />
                <h3 className="card-title">JS - приложение Slider</h3>
                <p>Стек: JS, ES6+, SCSS, адаптив</p>
              </a>
            </div>

            <div className="card">
              <a
                rel="noreferrer"
                href="https://cards-js.vercel.app/"
                className="card-link"
                target="_blank"
              >
                <img className="card-img" src={cards} alt="cards" />
                <h3 className="card-title">JS - приложение Cards</h3>
                <p>Стек: JS, ES6+, SCSS, адаптив</p>
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Main;
