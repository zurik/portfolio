import React from 'react';

import arrow from './../../assets/img/header/chevron-down.svg';

const Head = () => {
  return (
    <div className="header">
      <h1 className="header-title">Зураб Тхакохов</h1>
      <p className="header-subtitle">Frontend-разработчик, создание сайтов</p>
      <a href="#portfolio" className="header-arrow">
        <img src={arrow} alt="Arrow" />
      </a>
    </div>
  );
};

export default Head;
